import { ElectionResultsComponent } from './election-results.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const electionResultsRoutes: Routes = [
  { path: '', component: ElectionResultsComponent, data: {} },
];

@NgModule({
  imports: [RouterModule.forChild(electionResultsRoutes)],
  exports: [RouterModule],
  providers: []
})
export class ElectionResultsRoutes { }
