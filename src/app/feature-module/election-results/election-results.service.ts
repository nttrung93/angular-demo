import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Socket} from 'ngx-socket-io';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ElectionResultsService {

  constructor(
    private httpClient: HttpClient,
    private socket: Socket
    ) {

  }

  getChartData(): any {
    return this.socket
      .fromEvent('messages')
      .pipe(map((data) => data));
  }
}
