import { ElectionResultsRoutes } from './election-results-routing.module';
import { ElectionResultsComponent } from './election-results.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout';
import {NgxEchartsModule} from 'ngx-echarts';



@NgModule({
  declarations: [ElectionResultsComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    NgxEchartsModule,
    ElectionResultsRoutes,
  ]
})
export class ElectionResultsModule { }
