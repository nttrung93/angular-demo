import { TestBed } from '@angular/core/testing';

import { ElectionResultsService } from './election-results.service';

describe('ElectionResultsService', () => {
  let service: ElectionResultsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ElectionResultsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
