import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-election-results',
  templateUrl: './election-results.component.html',
  styleUrls: ['./election-results.component.css']
})
export class ElectionResultsComponent implements OnInit {

  chartOption: any;
  tomCount = 280;
  unvoted = 20;
  jerryCount = 240;

  constructor() { }

  ngOnInit(): void {
    this.initChart();
  }

  initChart(): void {
    this.chartOption = {
      grid: {
        left: '5%',
        right: '5%',
        bottom: '3%',
        containLabel: true
      },
      xAxis: [
        {
          type: 'value',
          axisLabel: false,
          axisTick: false,
          splitLine: false,
          max: 540
        }
      ],
      yAxis: [
        {
          type: 'category',
          axisTick: {
            show: false
          },
          data: ['Voted'],
          show: false
        },
      ],

      series: [
        {
          name: 'Tom',
          type: 'bar',
          stack: 'Voted',
          label: {
            show: true
          },
          data: [this.tomCount],
          barWidth: 50,
          itemStyle: {color: '#0664A0'},
        },
        {
          name: 'Unvoted',
          type: 'bar',
          stack: 'Voted',
          label: {
            show: false
          },
          data: [this.unvoted],
          itemStyle: {color: 'white'},
        },
        {
          name: 'Jerry',
          type: 'bar',
          stack: 'Voted',
          label: {
            show: true
          },
          data: [this.jerryCount],
          itemStyle: {color: '#CB1519'},
          markLine: {
            data: [
              { name: 'xxxx', xAxis: 270 }
            ],
            label: {
              formatter: '{c} NEED TO WIN'
            }
          },
        },

      ]
    };
  }
}
