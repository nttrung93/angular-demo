import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-prediction',
  templateUrl: './prediction.component.html',
  styleUrls: ['./prediction.component.css']
})
export class PredictionComponent implements OnInit {

  chartOption: any;

  constructor() {
  }

  ngOnInit(): void {
    this.initChart();
  }

  initChart(): void {
    this.chartOption = {
      xAxis: {
        splitLine: false,
        axisTick: false,
        type: 'category',
        data: ['Nov 1', 'Nov 2', 'Nov 3', 'Nov 4', 'Nov 5', 'Nov 6', 'Nov 7', 'Nov 8', 'Nov 9', 'Nov 10', 'Nov 11', 'Nov 12', 'Nov 13', 'Nov 14', 'Nov 15']
      },
      yAxis: {
        axisLine: {
          lineStyle: {
            opacity: 0
          }
        },
        splitLine: false,
        axisTick: false,
        type: 'value'
      },
      series: [
        {
          data: [0, 20, 40, 60, 90, 130, 150, 190, 200, 210, 220, 230],
          type: 'line',
          smooth: true,
          lineStyle: {
            color: '#0664A0',
            width: 4
          }
        },
        {
          data: [null, null, null, null, null, null, null, null, null, null, null, 230, 240, 250, 260],
          type: 'line',
          smooth: true,
          lineStyle: {
            color: '#0664A0',
            type: 'dashed',
            width: 4
          }
        },
        {
          data: [0, 15, 30, 55, 80, 120, 140, 170, 190, 195, 195, 198],
          type: 'line',
          smooth: true,
          lineStyle: {
            color: '#CB1519',
            width: 4
          }
        },
        {
          data: [null, null, null, null, null, null, null, null, null, null, null, 198, 205, 210, 215],
          type: 'line',
          smooth: true,
          lineStyle: {
            color: '#CB1519',
            type: 'dashed',
            width: 4
          }
        },
      ]
    };
  }
}
