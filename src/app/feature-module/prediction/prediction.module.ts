import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PredictionRoutingModule } from './prediction-routing.module';
import { PredictionComponent } from './prediction.component';
import {NgxEchartsModule} from 'ngx-echarts';


@NgModule({
  declarations: [PredictionComponent],
  imports: [
    CommonModule,
    NgxEchartsModule,
    PredictionRoutingModule
  ]
})
export class PredictionModule { }
