import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatSidenavModule} from '@angular/material/sidenav';

import {AppComponent} from './app.component';
import {HeaderComponent} from './shared/components/header/header.component';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {SocketIoConfig, SocketIoModule} from 'ngx-socket-io';
import {NgxEchartsModule} from 'ngx-echarts';
import {PageNotFoundComponent} from './feature-module/page-not-found/page-not-found.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {KeycloakAngularModule} from 'keycloak-angular';
import {AppAuthGuard} from './shared/guard/app.authguard';

const config: SocketIoConfig = {url: 'ws://localhost:3000', options: {}};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatToolbarModule,
    MatExpansionModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    FlexLayoutModule,

    HttpClientModule,
    SocketIoModule.forRoot(config),
    NgxEchartsModule.forRoot({
      /**
       * This will import all modules from echarts.
       * If you only need custom modules,
       * please refer to [Custom Build] section.
       */
      echarts: () => import('echarts'), // or import('./path-to-my-custom-echarts')
    }),
    KeycloakAngularModule
  ],
  providers: [
    AppAuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
