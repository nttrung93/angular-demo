import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {PageNotFoundComponent} from './feature-module/page-not-found/page-not-found.component';
import {AppAuthGuard} from './shared/guard/app.authguard';

const appRoutes: Routes = [
  {path: '', redirectTo: '/election', pathMatch: 'full'},
  {
    path: 'election', loadChildren: () => import('./feature-module/election-results/election-results.module')
      .then(m => m.ElectionResultsModule)
  },
  {
    path: 'prediction',
    loadChildren: () => import('./feature-module/prediction/prediction.module').then(m => m.PredictionModule),
    // canActivate: [AppAuthGuard],
    data: { roles: ['NormalUser'] }
  },
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false, // <-- debugging purposes only
      }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
