import { KeycloakConfig } from 'keycloak-angular';

const keycloakConfig: KeycloakConfig = {
  url: 'http://KEYCLOAK-IP:8088/auth',
  realm: 'your-realm-name',
  clientId: 'your-client-id',
  credentials: {
    secret: 'your-client-secret'
  }
};

export const environment = {
  production: true,
  keycloak: keycloakConfig,
};
